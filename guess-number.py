#!/usr/bin/python3

# initilize variables
secret = 1337
guess = -1
counter = 0

while guess != secret:

    # ask person to give an answer...
    guess = input("What is your guess?")

    # make sure the answer is an integer
    guess = int(guess)

    if guess < secret:
        print("Too low! Try again!")

    if guess > secret:
        print("Too high! Try again!")

    if guess == secret:
        print("Correct!, The answer is", secret)

    counter = counter + 1

print("It took you", counter, "tries to guess the correct number!")

# EOF guess-number.py
