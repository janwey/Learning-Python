#!/usr/bin/python3

# Chapters 1 - 4 --------------------------------------------------------------
# {{{

# create dictionary with 2 values v1 & v2
d = {"key1": "v1", "key2": "v2"}
d

# add another value to dictionary
d["key3"] = "v3"
d

# create variables
name123 = "testing"
list123 = [1, 2, 3]
list123

# logics
3 < 4
3 != 4
name123 == "testing"

a = 1 + 2
b = 12.5 / 4
(a - 7) < (b * b + 6.5)

# reverse logics
not (4 < 3)
not (3 < 4)

# chain logics
(3 < 4) or (5 < 6)
(3 < 4) or (4 < 3)
(5 > 6) or (4 < 3)

# functions
abc = [1, 2, 3, 4, 5, 6, 7, 99]
max(abc)
max(1, 2, 3)

# methods
liste = [2, 5, 9, 45, 23, 1, 4, 6, 89]
liste.sort()  # apply sort() on liste
liste

"Hello World".count("l")  # how many "l" are in the string?

# print results
print(1.2)
print("Hello World")
var = 42
print("The magic number is:", var)

# advanced logics
x = 11
if x > 10:
    print(x, "is greater than 10!")
    print("I guess, that should be clear...")

# line-wrapping
var0 \
    = \
    123
print(var0)

"Hello " \
    "World"

# 1-liners
print("Hello"); print("World!")

x = True
if x: print("Hello"); print("World")

# NOW DO ./guess-number.py :)

# }}}

# Chapters 5 - 6 --------------------------------------------------------------
# {{{

# if, elif and else
x = 1
if x == 1:
    print("x has value 1")

x = 2
if x == 1:
    print("x has value 1")
elif x == 2:
    print("x has value 2")

x = 3
if x == 1:
    print("x has value 1")
elif x == 2:
    print("x has value 2")
else:
    print("X has neither value 1 nor value 2")

# conditioned assignments
x = 3
if x == 1:
    var = 20
else:
    var = 30
print(var)

var = (20 if x == 1 else 30)
print(var)

print("x has value 1" if x == 1 else "x has not value 1")

# while-loops
secret = 1337
trial = -1
while trial != secret:
    trial = int(input("Take a guess: "))
    # some breakage-condition
    if not(trial > 0):
        print("Only values greater than 0, please!")
        break
else:
    # if guess is correct
    print("You did it!")

while True:
    number = int(input("Give a number: "))
    if number < 0:
        print("Negatives are not allowed!")
        continue
    result = 1
    while number > 0:
        result = result * number
        number = number - 1
    print("Result: ", result)

# for-loops
for i in [1, 2, 3]:
    print(i)

for c in "JayVii learns Python":
    print(c)

# ranges
start = 0
stop = 101
step = 5
range(start, stop, step)

for i in range(start, stop, step):
    print(i)

for i in range(10, 1, -2):
    print(i)

# pass / skipping
for i in [1, 2, 3]:
    if i == 1:
        pass  # skipps this / does nothing
    else:
        print("it is the value", i)

# reading data streams
fobj = open("./somefile.txt", "r")  # read (r) file
for line in fobj:
    print(line)
fobj.close()  # stop reading from file

dictionary = {}  # empty object
fobj = open("./somefile.txt", "r")
for line in fobj:
    line = line.strip()  # remove "\n" from each line
    assignment = line.split(" ")  # split at space
    dictionary[assignment[0]] = assignment[1]
fobj.close()  # stop reading from file
print(dictionary)

while True:
    word = input("What do you want to translate? ")
    if word in dictionary:
        print("The german word for", word, "is", dictionary[word])
    else:
        print("Sorry, unknown word!")

# with-command
with open("./somefile.txt", "r") as fobj2:
    for line in fobj2:
        print(line)
# close() not necessary when using with

# writing data streams
fobj = open("./someoutput.txt", "w")
for engl in dictionary:
    fobj.write("{} {}\n".format(engl, dictionary[engl]))
fobj.close()

# when using open(), following parameters can be used:
# - r: read
# - w: write
# - a: append

# continue at http://openbook.rheinwerk-verlag.de/python/07_001.htmly

# }}}

# Chapter 7 -------------------------------------------------------------------
# {{{

# instances and references
a = 1337
2674 / a
# a references specific integer value (1337)

b = a
# now b is also a reference for 1337, however only one instance of 1337 is being
# stored in RAM, while having two references: a & b.

b = a * 2
# now b is a reference for the instance 2674 and not for the instance 1337
# anymore. We now have two instances (1337 & 2674) with 1 reference each.

# data types
type(1337)
type("Hello World!")
type(b)

type(a) == type(b)
type(a) == int
type(a) == str

# re-assignments change the data type!
c = "i am a string!"
type(c)
c = 123
type(c)

# comparing instances
somefloat = 123.0
type(somefloat)
someint = 123
type(someint)
somefloat == someint
# numeric values can be compared regardless

somestring = "123"
type(somestring)
someint == somestring
# this is not the case for numerics + strings

# identities
v1 = [1, 2, 3]
v2 = [1, 2, 3]
v3 = v1
id(v1)
id(v2) == id(v1)
id(v3) == id(v1)
# v2 has a different identity, eventhough it is the exact same value.
# Assignments do matter!

# but what if v3 is changed? does it change the instance or create a new
# identity?
id(v1) == id(v3)
v3[2] = 4
v1
id(v1) == id(v3)
# indeed, it chances the instance!

# 7.2 --------------------------------------------------------------------------
# deleting instances and references
msg = "Welcome to this script!"
print(msg)
del(msg)
print(msg)
# this only deletes the reference "msg", not the instance! The instance is
# automatically deleted by the garbage collector if no references point to it
# anymore (effectively, this DOES delete the instance).

# delete multiples
v1 = 1
v2 = 2
v3 = 3
del v1, v2, v3

# 7.3 --------------------------------------------------------------------------

# immutable data types
a = 1
b = 1
c = [1, 2]
d = [1, 2]
a is b
c is d
# why is the same instance used?
# single integers are immutable (un-changable) unlike some vector. hence
# RAM-space can be safed by using the same instance.

# mutable data types
a = "water"
a += " bottle"
a

a = [1, 2]
a += [3, 4]
a

# when adding to some string, instances may change
a = "water"
b = a
a is b
a += " bottle"
a is b
a; b

# when adding to some vector, instances do not change
a = [1, 2]
b = a
a is b
a += [3, 4]
a is b
a; b

# the string is immutable, hence by adding " bottle" to it, a new instance is
# created. the vector is mutable, so adding to it behaves as expected.
a = "water"
id(a)
a += " bottle"
id(a)

# }}}

# Chapter 8 -------------------------------------------------------------------
# {{{

# functions
val = max([3, 6, 9])
val / 2

# methods
list = [4, 235, 68, 12, 1, 87]
list.sort()
list

# parameters
#var = 12
#ref.method(var, "Hello World!")
# method() has two parameters in this example
# if the method is defined as method(param1, param2, param3), one may also call
# it via ref.method(param1=val1, param2=val2, val3), keys are not necessarily
# needed in this case.
# parameters may also be OPTIONAL
# if the method is defined as method(param1, {param2}), then the key param2 is
# necessary for setting the second parameter.

# attributes
# every complex number has the attributes "real" and "imag"
num = 5 + 6j
num.real
num.imag

# attributes are references by themselves
num.real * num.real + num.imag
[1, num.real, num.imag]

# the instance refered by ref.attribute may also have attributes by itself:
# ref.attribute.attr
# functions can be applied to it the same way
num.real.is_integer()

# }}}

# Chapter 9 -------------------------------------------------------------------
# {{{

# help and documentation
help()

help(for)  # required in Debian: python3-doc

# help about imports
import pprint
help(pprint.pprint)

# strings also work!
import copy
help("copy.copy")

# online documentation via https://docs.python.org

# }}}

# EOF basics.py
